const blockchain = require ('./blockchain.js')
const express = require('express')
const app = express()
const uuid = require('uuid')
const nodeAddress = uuid().split('-').join(' ')
const bodyParser = require('body-parser')
const bitcoin = new blockchain()
const port = process.argv[2]
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))
app.get('/blockchain', (req, res)=>{
    res.send(bitcoin);
})
app.post('/transaction', (req, res)=>{
    const blockIndex = bitcoin.createNewTransaction(req.body.amount, req.body.sender, req.body.recipient)
    res.json({ note:`Transaction will be added in block ${blockIndex}.` })
})
app.get('/mine', (req, res)=>{
    const lastBlock = bitcoin.getLastBlock()
    const previousBlockHash = lastBlock['hash']
    const currentBlockData = {
        transactions: bitcoin.pendingTransaction,
        index: lastBlock['index'] + 1
    }
    const nonce = bitcoin.proofOfWork(previousBlockHash, currentBlockData)
    const hash = bitcoin.hashBlock(previousBlockHash, currentBlockData, nonce)
    bitcoin.createNewTransaction(12.5, "00", nodeAddress)
    const newBlock = bitcoin.createNewBlock(nonce, previousBlockHash, hash)
    res.json({
        note: "New block mined successfully",
        block: newBlock
    })
})
app.post('/register-and-broadcast-node', (req, res)=>{
    const newNodeUrl = req.body.newNodeUrl
})
app.post('/register-node', ()=>{
    
})
app.listen(port,()=>{
    console.log(`Listening on port ${ port }`)
})